import { useRouter } from "next/router";
import Link from "next/link";
import { server } from "../../../config";
import { GetStaticProps, GetStaticPaths } from "next";
import { Article } from "../../../models/interfaces";

const ArticleItem = ({ article }) => {
  return (
    <div className="container">
      <h2>{article.title}</h2>
      <p>{article.body}</p>
      <Link href="/"> Go Back </Link>
    </div>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  const res = await fetch(`${server}/api/articles/${context?.params?.id}`);
  const article = await res.json();
  return {
    props: { article },
  };
};

export const getStaticPaths: GetStaticPaths = async (context) => {
  const res = await fetch(`${server}/api/articles`);
  const articles = await res.json();
  const ids = articles.map((article: Article) => article.id);
  const paths = ids.map((id) => ({ params: { id: id.toString() } }));
  return {
    paths,
    fallback: false,
  };
};

export default ArticleItem;
