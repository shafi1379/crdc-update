// import getConfig from "next/config";
import Head from "next/head";
import Image from "next/image";
import Layout from "../components/layout";
import { server } from "../config";
import { GetStaticProps } from "next";
// Only holds serverRuntimeConfig and publicRuntimeConfig
// const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();
// Will only be available on the server-side
// console.log(serverRuntimeConfig.mySecret);
// Will be available on both server-side and client-side
// console.log(publicRuntimeConfig.staticFolder);

import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export default function Home(props) {
  return (
    <div className="container">
      <Layout schoolList={props.schoolList} fsc={props.fsc} />
    </div>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  const res = await loadSchools()
    .catch((e) => {
      throw e;
    })
    .finally(async () => {
      await prisma.$disconnect();
    });
  //   const res = await fetch(`${server}/api/articles`);
  //   const articles = await res.json();
  return {
    props: { schoolList: res.schools, fsc: res.fsc },
  };
};

const loadSchools = async () => {
  const res = await prisma.schoolForm1.findMany({
    select: {
      LEAID: true,
      SCHID: true,
      SCHOOL_NAME: true,
    },
  });
  const schoolList = res.map((data) => {
    return {
      label: `${data.SCHID} - ${data.SCHOOL_NAME}`,
      value: `${data.SCHID} - ${data.SCHOOL_NAME}`,
    };
  });
  return { schools: schoolList, fsc: res[0].LEAID };
};
