import { articles } from "../../../datas/data";
import { NextApiRequest, NextApiResponse } from "next";
import { Article } from "../../../models/interfaces";

export default (req: NextApiRequest, res: NextApiResponse<Article[]>) => {
  res.status(200).json(articles);
};
