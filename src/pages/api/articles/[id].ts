import { articles } from "../../../datas/data";
import { NextApiRequest, NextApiResponse } from "next";
import { Article } from "../../../models/interfaces";

export default function handler(
  { query: { id } }: NextApiRequest,
  res: NextApiResponse
) {
  const filtered = articles.filter(
    (article: Article) => article.id.toString() === id
  );
  if (filtered.length > 0) {
    res.status(200).json(articles[0]);
  } else {
    res.status(404).json({ massage: `Article with id of ${id} is not found.` });
  }
}
