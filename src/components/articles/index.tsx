import Link from "next/link";
import { Article } from "../../models/interfaces";

export default function Articles({ schoolList }) {
  return (
    <div className="grid">
      {schoolList.map((article: Article) => (
        <Link
          key={article.id}
          href="/article/[id]"
          as={`/article/${article.id}`}
        >
          <a className="card">
            <h2>{article.title}</h2>
            <p>{article.body}</p>
          </a>
        </Link>
      ))}
    </div>
  );
}
