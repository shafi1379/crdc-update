import Head from "next/head";
import Image from "next/image";
import Header from "./header";
import Contents from "./contents";
import Footer from "./footer";

export default function Layout(props) {
  return (
    <>
      <Header />
      <Contents schoolList={props.schoolList} fsc={props.fsc} />
      <Footer />
    </>
  );
}
