import Head from "next/head";
import Image from "next/image";
import Paper from "@material-ui/core/Paper";
import Select from "react-select";
import { Button, Typography } from "@material-ui/core";
import { Print, Save } from "@material-ui/icons";

export default function Contents(props) {
  return (
    <main className="main">
      <Paper className="paper">
        <div
          style={{
            padding: 20,
            display: "flex",
            flexGrow: 1,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-around",
          }}
        >
          <div
            className="paper-item"
            style={{ display: "flex", alignItems: "center" }}
          >
            <Typography variant="subtitle2" style={{ marginRight: 5 }}>
              <strong>School:</strong>
            </Typography>
            <Select
              value={props.schoolList[0]}
              options={props.schoolList}
              styles={{
                width: 200,
                menuList: (base) => ({
                  ...base,
                  maxHeight: 400,
                }),
                menuPortal: (base) => ({ ...base, zIndex: 5 }),
              }}
              className="dropdown-select"
            />
          </div>
          <div
            className="paper-item"
            style={{ display: "flex", alignItems: "center" }}
          >
            <Typography variant="subtitle2" style={{ marginRight: 5 }}>
              <strong>Federal School Code:</strong>
            </Typography>
            <Typography variant="body2">{props.fsc}</Typography>
          </div>
          <div className="paper-item">
            <Button
              variant="contained"
              color="primary"
              size="small"
              startIcon={<Save />}
            >
              Save
            </Button>
            <Button
              style={{ marginLeft: 15 }}
              variant="contained"
              color="primary"
              size="small"
              startIcon={<Print />}
            >
              Print
            </Button>
          </div>
        </div>
        <hr />
        <div>
          
        </div>
      </Paper>
    </main>
  );
}
