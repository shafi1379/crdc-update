import Head from "next/head";
import Image from "next/image";

const getCurrentYear = new Date().getFullYear();

export default function Footer() {
  return (
    <footer className="footer">
      ©{getCurrentYear}
      <a href="http://www.ushasoftware.com" target="_blank" rel="noreferrer">
        &nbsp;USHA Software&nbsp;
      </a>
      All Rights Reserved&nbsp;
      <span className="version">V20200730</span>
    </footer>
  );
}
