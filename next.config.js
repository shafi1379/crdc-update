const path = require("path");
const { PHASE_DEVELOPMENT_SERVER } = require("next/constants");

module.exports = (phase, { defaultConfig }) => {
  if (phase === PHASE_DEVELOPMENT_SERVER) {
    return {
      optimizeFonts: false,
      productionBrowserSourceMaps: true,
      reactStrictMode: true,
      compress: false,
      basePath: "",
      pageExtensions: ["ts", "tsx"],
      sassOptions: {
        includePaths: [path.join(__dirname, "styles/_styles")],
      },
      eslint: {
        dirs: ["pages", "components", "models"], // Only run ESLint on the 'pages' and 'utils' directories during production builds (next build)
      },
      env: {
        customKey: "my-value",
      },
      //      serverRuntimeConfig: {
      //        // Will only be available on the server side
      //        mySecret: "secret",
      //        secondSecret: process.env.SECOND_SECRET, // Pass through env variables
      //      },
      //      publicRuntimeConfig: {
      //        // Will be available on both server and client
      //        staticFolder: "/static",
      //      },
      distDir: "build",
      generateBuildId: async () => {
        // You can, for example, get the latest git commit hash here
        return "my-build-id";
      },
      eslint: {
        // Warning: Dangerously allow production builds to successfully complete even if
        // your project has ESLint errors.
        ignoreDuringBuilds: true,
      },
      typescript: {
        // !! WARN !!
        // Dangerously allow production builds to successfully complete even if
        // your project has type errors.
        // !! WARN !!
        ignoreBuildErrors: true,
      },
      trailingSlash: false,
    };
  }
  return {
    reactStrictMode: true,
  };
};
